solidserver_space  = "UGA"
solidserver_subnet = "SPR-OPENSTACK-03"
dns_domain         = "u-ga.fr."
dns_servers        = ["152.77.1.22", "195.83.24.30"]


image_name      = "ubuntu-18.04-docker-x86_64"
public_net_name = "public"
secgroup_rules = [{ "source" = "152.77.119.207/32", "protocol" = "tcp", "port" = 22 },
  { "source" = "152.77.119.207/32", "protocol" = "icmp", port = 0 },
  { "source" = "147.171.168.176/32", "protocol" = "icmp", port = 0 },
  { "source" = "152.77.119.207/32", "protocol" = "tcp", "port" = 6443 },
  { "source" = "147.171.168.176/32", "protocol" = "tcp", "port" = 22 },
  { "source" = "147.171.168.176/32", "protocol" = "tcp", "port" = 6443 },
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 80 },
  { "source" = "0.0.0.0/0", "protocol" = "tcp", "port" = 443 }
]
master_flavor_name = "m1.small"
worker_flavor_name = "m1.large"
worker_count       = "4"

#kubernetes_version = "v1.15.11-rancher1-3"

storage_types   = ["solidfire", "ceph"]
default_storage = "ceph"
